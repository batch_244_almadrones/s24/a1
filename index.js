//[Item 1]
let getCube, num, power;
num = 2;
power = 3;
getCube = Math.pow(2, 3);
console.log(`The cube of ${num} is ${getCube}.`);

//[Item 2]
let address = ["258 Washington Ave NW", "California 90011"];
let [streetName, city] = address;
console.log(`I live at ${streetName}, ${city}.`);

//[Item 3]
let animal = {};
animal.petname = "Lolong";
animal.kind = "salt water crocodile";
animal.weight = 1075;
animal.len = "20 ft 3 in";
let { petname, kind, weight, len } = animal;
console.log(
  `${petname} was a ${kind}. He weighed at ${1075} kgs with a measurement of ${len}.`
);

//[Item 4]
let arrNum = [1, 2, 3, 4, 5];
arrNum.forEach((num) => console.log(num));
console.log(arrNum.reduce((x, y) => x + y));

//[Item 5]
class Dog {
  constructor(dogName, age, breed) {
    this.dogName = dogName;
    this.age = age;
    this.breed = breed;
  }
}

let shihTzu = new Dog("Tottie", "3", "Shih Tzu");
console.log(shihTzu);
